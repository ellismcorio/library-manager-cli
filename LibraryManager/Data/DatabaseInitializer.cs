﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryManager.Models;

namespace LibraryManager.Data
{
    public class DatabaseInitializer : DropCreateDatabaseAlways<Context>
    {
        protected override void Seed(Context context)
        {

            var book1 = new Book()
            {
                Id = 1,
                Title = "Physical Examination and Health Assessment",
                Author = "Carolyn Jarvis",
                Description = "A book containing everything you need to know regarding conducting health assesments",
                Publisher = "Saunders",
                PublishedOn = "2015-02-18",
            };

            var book2 = new Book()
            {
                Id = 2,
                Title = "How Not to Die: Discover the Foods Scientifically Proven to Prevent and Reverse ",
                Author = " Michael Greger M.D.",
                Description = "Contains information regarding the health benefits of food",
                Publisher = "Saunders",
                PublishedOn = " 2015-12-08",
            };

            var book3 = new Book()
            {
                Id = 3,
                Title = "The Ultimate Pet Health Guide: Breakthrough Nutrition and Integrative Care for Dogs and Cats",
                Author = "Gary Richter MS DVM",
                Description = "A Book containing nutritional information for dogs.",
                Publisher = " Cengage Learning",
                PublishedOn = "2017-08-15",
            };

            var book4 = new Book()
            {
                Id = 4,
                Title = "Medical Terminology for Health Professions, Spiral bound Version",
                Author = " Katrina A. Schroeder",
                Description = "A Book containing medical terminology",
                Publisher = "Jones & Bartlett Learning",
                PublishedOn = "2016-01-01",
            };


            var book5 = new Book()
            {
                Id = 5,
                Title = "Jonas and Kovner's Health Care Delivery in the United States, 11th Edition",
                Author = "Richard Skolnik",
                Description = "A Book containing detailed information regarding healthcare in America",
                Publisher = "Springer Publishing Company",
                PublishedOn = "2016-01-01",
            };

            var book6 = new Book()
            {
                Id = 6,
                Title = "The Hidden Tools of Comedy: The Serious Business of Being Funny",
                Author = "Steve Kaplan",
                Description = "A Book about the art of comedy",
                Publisher = " Michael Wiese Productions",
                PublishedOn = "2013-07-01",
            };


            var book7= new Book()
            {
                Id = 7,
                Title = "Mastering Stand-Up",
                Author = " Stephen Rosenfield",
                Description = "A Bookabout becoming a sucsesfull comedian",
                Publisher = "Race Point Publishing",
                PublishedOn = "2015-09-21",
            };

            var book8 = new Book()
            {
                Id = 8,
                Title = "Truth in Comedy: The Manual for Improvisation",
                Author = " Kim Howard Johnson",
                Description = "A Book about improvisational comedy",
                Publisher = "Meriwether Pub",
                PublishedOn = "1994-04-01",
            };

            var book9 = new Book()
            {
                Id = 9,
                Title = "The Travelling Cat Chronicles",
                Author = "Hiro Arikawa",
                Description = "A Book about cats travelling the worldy",
                Publisher = " HarperCollins",
                PublishedOn = "2018-10-23",
            };

            var book10 = new Book()
            {
                Id = 10,
                Title = "I Could Pee on This: And Other Poems by Cats",
                Author = " Francesco Marciuliano",
                Description = "A Book of poems about cats",
                Publisher = " Chronicle Books",
                PublishedOn = "2012-08-15",
            };

            var book11 = new Book()
            {
                Id = 11,
                Title = "How to Speak Cat: A Guide to Decoding Cat Language",
                Author = "Gary Weitzman DVM MPH",
                Description = "A Book regarding understanding your cat",
                Publisher = "National Geographic Children's Books",
                PublishedOn = "2015-01-06",
            };

            var book12 = new Book()
            {
                Id = 12,
                Title = "How to Speak Cat: A Guide to Decoding Cat Language",
                Author = "Gary Weitzman DVM MPH",
                Description = "A Book regarding understanding your cat",
                Publisher = "National Geographic Children's Books",
                PublishedOn = "2015-01-06",
            };

            var book13 = new Book()
            {
                Id = 13,
                Title = " How to Talk to Your Cat About Gun Safety: And Abstinence, Drugs, Satanism, and Other Dangers That Threaten Their Nine Lives",
                Author = "James Dean",
                Description = "A Book about educating your cat on modern day dangers",
                Publisher = " HarperFestival",
                PublishedOn = "2017-09-26",
            };

            var book14 = new Book()
            {
                Id = 14,
                Title = "Save the Cat! Writes a Novel: The Last Book On Novel Writing You'll Ever Need",
                Author = " Blake Snyder",
                Description = "A Book about educating your catabout novel writing",
                Publisher = "Michael Wiese Productions",
                PublishedOn = "2005-05-25",
            };

            var book15 = new Book()
            {
                Id = 15,
                Title = "The Illustrated World Encyclopedia of Guns: Pistols, Rifles, Revolvers, Machine And Submachine Guns Through History In 1100 Clear Photographs",
                Author = "Will Fowler",
                Description = "A Book about guns",
                Publisher = " DK",
                PublishedOn = "2014-03-17",
            };

            var book16 = new Book()
            {
                Id = 16,
                Title = "The Southerner's Cookbook: Recipes, Wisdom, and Stories",
                Author = "Editors of Garden and Gun",
                Description = "A Book with recipes, guns and stories",
                Publisher = "Harper Wave",
                PublishedOn = "2017-04-01",
            };

            var book17 = new Book()
            {
                Id = 17,
                Title = "Guns, Germs, and Steel: The Fates of Human Societies",
                Author = "Jared Diamond Ph.D.",
                Description = "A Book about guns,war and germss",
                Publisher = " W. W. Norton & Company",
                PublishedOn = "2017-03-07",
            };

            var book18 = new Book()
            {
                Id = 18,
                Title = "History of the Gun in 500 Photographs",
                Author = "TIME-LIFE Books",
                Description = "A Book about the history of guns",
                Publisher = "TIME-LIFE",
                PublishedOn = "2016-05-10",
            };

            var book19 = new Book()
            {
                Id = 19,
                Title = "Gun Dog: Revolutionary Rapid Training Method",
                Author = "Richard A. Wolters",
                Description = "A Book about training gun dogs",
                Publisher = " Dutton",
                PublishedOn = "1961-05-26",
            };


            var book20 = new Book()
            {
                Id = 20,
                Title = "Run to the Sound of the Guns: The True Story of an American Ranger at War in ",
                Author = "Nicholas Moore",
                Description = "A Book about the history of american rangers",
                Publisher = "Osprey Publishing",
                PublishedOn = "2018-11-13",
            };

            context.Book.Add(book1);
            context.Book.Add(book2);
            context.Book.Add(book3);
            context.Book.Add(book4);
            context.Book.Add(book5);
            context.Book.Add(book6);
            context.Book.Add(book7);
            context.Book.Add(book8);
            context.Book.Add(book9);
            context.Book.Add(book10);
            context.Book.Add(book11);
            context.Book.Add(book12);
            context.Book.Add(book13);
            context.Book.Add(book14);
            context.Book.Add(book15);
            context.Book.Add(book16);
            context.Book.Add(book17);
            context.Book.Add(book18);
            context.Book.Add(book19);
            context.Book.Add(book20);
            context.SaveChanges();
        }
    }
}
