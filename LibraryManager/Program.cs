﻿using LibraryManager.Helpers;
using LibraryManager.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using LibraryManager.Models;

namespace LibraryManager
{
    class Program
    {
         public static Dictionary<int, string> openWith =
         new Dictionary<int, string>();     // create a dictionary so that key value pairs can be iterated over

        static void Main()
        {
            const string CommandAll = "1";      //defines some command variables
            const string CommandAdd = "2";
            const string CommandQuit = "quit";

            Console.Clear();
            ConsoleHelper.OutputBlankLine();
            Console.WriteLine("Welcome to the Library Manager!");
            ConsoleHelper.OutputBlankLine();
            ConsoleHelper.OutputBlankLine();
            Console.WriteLine("To View all Books please enter: " + CommandAll);
            ConsoleHelper.OutputBlankLine();
            Console.WriteLine("To Add a Book Please enter: " + CommandAdd);
            ConsoleHelper.OutputBlankLine();
            Console.WriteLine("To Quit please type quit.");

            using (var context = new Context())
            {
                string input = "";
                while (input != CommandQuit)
                {
                    input = Console.ReadLine();

                    if (input == CommandQuit)   //if user quits the program
                    {
                        System.Environment.Exit(1);
                    }

                    if (input == CommandAll)    //if userwants to view all books
                    {
                        Console.Clear();
                        View(context);
                        input = Console.ReadLine();
                    }
               
                    if (input == CommandAdd)    // if user wants to add a new book to the library
                    {
                        Console.Clear();
                        var newBook = new Book();
                        Console.WriteLine("Please enter the Name of the book you wish to add");
                        input = Console.ReadLine();
                        newBook.Title = input;
                        Console.WriteLine("Please enter the Author of the book you wish to add");
                        input = Console.ReadLine();
                        newBook.Author = input;
                        Console.WriteLine("Please enter the Description of the book you wish to add");
                        input = Console.ReadLine();
                        newBook.Description = input;
                        Console.WriteLine("Please enter the Publisher of the book you wish to add");
                        input = Console.ReadLine();
                        newBook.Publisher = input;
                        Console.WriteLine("Please enter the Published on date of the book you wish to add");
                        input = Console.ReadLine();
                        newBook.PublishedOn = input;
                        context.Entry(newBook).State = EntityState.Added;
                        context.SaveChanges();                  
                        View(context);
                    }
                }
            }
        }

        public static void View(Context context)
        {
            const string CommandBack = "b";
            const string CommandQuit = "quit";
            const string CommandMain = "main";
            const string CommandDelete = "delete";
           

            var bookList = context.Book.ToList();   //create a list of books in the database to iterate over
            Console.Clear();
            if (openWith.Count > 0)     //if the the dictionary already has books in it
            {
                for (int i = 1; i < bookList.Count+1; i++)      //iterate over the collection
                {
                    Console.WriteLine(i + " " + bookList[i-1].Title);       //display a numbered list of book from the dictionary
                }
            }
            else      //if the dicitonary  doent not have any books in it. 
            {
                for (int i = 1; i < bookList.Count+1; i++)      //iterate over the collection
                {
                    openWith.Add(i, bookList[i-1].Title);       //adds the booklist to the dictionarty
                    Console.WriteLine(i + " " + bookList[i-1].Title);        //display a numbered list of book from the dictionary
                }
            }

            ConsoleHelper.OutputBlankLine();
            Console.WriteLine("Please enter the number of the book to see details: ");
            Console.WriteLine("To Quit please type quit.");
            Console.WriteLine("To go back to the main menu please type main.");
        
            string response = "";
            while (response != "q")
            {
                if (response == CommandMain)        //if the user wants to return to the main menu
                {
                    Main();
                }
                response = Console.ReadLine();      //stores the user input to a response variable
                int enteredID = 0;
                if (int.TryParse(response, out enteredID))      //parsese the response and stores the passed value in a new variable
                {
                    foreach (var item in openWith) // loop through the dictionary
                    {
                        if (item.Key == enteredID)      //compares the parsed value with the key of an item in the dictionary
                        {
                            //item.Value = book title
                            foreach (var book in bookList) // loop through booklist
                            {
                                if (book.Title == item.Value) // check booklist book title against dictionary value
                                {
                                  

                                    //output details of book
                                    Console.Clear();
                                    ConsoleHelper.OutputBlankLine();


                                    List<List<string>> bookslist = new List<List<string>>();        //creates a list in order to display the propertys of the book 
                                    bookslist.Add(new List<string>() { "1 ", "Title: ", book.Title});       //passing in the valuesof the book in question with a key/property/value
                                    bookslist.Add(new List<string>() { "2 ", "Description: ", book.Description});
                                    bookslist.Add(new List<string>() { "3 ", "Author: ", book.Author});
                                    bookslist.Add(new List<string>() { "4 ", "Publisher: ", book.Publisher});
                                    bookslist.Add(new List<string>() { "5 ", "PublishedOn: ", book.PublishedOn});

                                    foreach (var thing in bookslist)
                                    {
                                        Console.WriteLine(thing[0] + thing[1] + thing[2]);      //for each book property display the key/property/value
                                    }

                                    ConsoleHelper.OutputBlankLine();

                                    string responseBookEdit = "";       //sets up a variable to temporarily store new values when editing a books values
                                    while (responseBookEdit != CommandQuit)
                                    {
                                        Console.WriteLine("To Quit please type quit.");
                                        Console.WriteLine("To Edit this book please type the relevant line number.");
                                        Console.WriteLine("To go back to the main menu please type main.");
                                        Console.WriteLine("To delete this book please type delete.");
                                        responseBookEdit = Console.ReadLine();

                                        if (responseBookEdit == CommandQuit)        //if the user wishes to quit
                                        {
                                            System.Environment.Exit(1);     //exits the app
                                        }
                                       
                                        else if (responseBookEdit == CommandMain)        //if the user wishes to return to the main menu
                                        {
                                            Main();     //renders the initial page
                                        }
                                        else if (responseBookEdit == CommandDelete)     // if the user wishes to return to delete a book
                                        {
                                            var option = bookList[int.Parse(response)];     // finds the book to delete
                                            context.Entry(option).State = EntityState.Deleted;      // sets the entity state to deleted
                                            context.SaveChanges();      // saves the changes to the context
                                        }
                                        else if (responseBookEdit != CommandQuit || responseBookEdit == CommandBack || responseBookEdit == CommandDelete)
                                        {            //if response is anything other thann a predifined option
                                            Console.WriteLine("Please make a valid selection");
                                        }

                                        int responseID;     // temporary variable for storing a valuie of a line to be edited
                                        int lineID;         // variable to store to id of the line to be edited

                                        foreach (var line in bookslist)     // iterate overall available properties to find the one that matches the property to be edited
                                        {
                                            bool responseParsed = int.TryParse(responseBookEdit, out responseID);       // boolean to identify if a query to edit a line has been parsed
                                            bool lineParsed = int.TryParse(line[0], out lineID);                        // boolean to identify that a line value for a property has been parsed

                                            if (responseParsed && lineParsed && (responseID == lineID))                 // if parsed values for a query to edit a property and a line value 
                                            {                                                                           // for that property match....
                                                Console.WriteLine("Please enter a new value for the selected property.");
                                                var newValue = Console.ReadLine();                                      // assigns the new value to a variable
                                                
                                                var option = bookList[int.Parse(response)-1];      // find the id of selected book

                                                var a = context.Book.SingleOrDefault(b => b.Id == option.Id);       // create a new book with the same id

                                                if (responseID == 1)        // if the user wishes to edit the title
                                                {
                                                    a.Title = newValue;     // assign the new value to the title
                                                }

                                                else if (responseID == 2)        // if the user wishes to edit the description
                                                {
                                                    a.Description = newValue;       // assign the new value to the description
                                                }

                                                else if(responseID == 3)        // if the user wishes to edit the author
                                                {
                                                    a.Author = newValue;        // assign the new value to the author
                                                }

                                                else if(responseID == 4)        // if the user wishes to edit the publisher
                                                {
                                                    a.Publisher = newValue;     // assign the new value to the publisher
                                                }

                                                else if(responseID == 5)        // if the user wishes to edit the published date
                                                {
                                                    a.PublishedOn = newValue;       // assign the new value to the published on date
                                                }
                                                else
                                                {
                                                    Console.WriteLine("Please enter a valid property.");        // if a user enters a non valid choice
                                                }
                                                context.Entry(a).CurrentValues.SetValues(a);        // sets the new values to the entry in the context

                                                context.Entry(a).State = EntityState.Modified;      // sets the entity state to modified  

                                                context.SaveChanges();      //saves the changes to the context
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (response == CommandQuit)        // if the user wishes to quit the program
                    {
                        System.Environment.Exit(1);     // exits the application
                    }
                    //give error to user
                    Console.WriteLine("That is not a valid option.");       //informsthe user that they have entered a non valid option
                }
            }
        }
    }
}
